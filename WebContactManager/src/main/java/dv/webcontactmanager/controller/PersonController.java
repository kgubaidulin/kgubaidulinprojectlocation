/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.webcontactmanager.controller;

import dv.webcontactmanager.model.Person;
import dv.webcontactmanager.service.PersonService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author k.gubaidulin
 */

@Controller
public class PersonController {
    
    @Autowired
    private PersonService personService;
    
    @RequestMapping("/index")
    public String listPersons(Map<String, Object> map) {
        map.put("person", new Person());
        map.put("personList", personService.listPerson());
        
        return "contact";
    }
    
    @RequestMapping("/")
    public String home() {
        return "redirect:/index";
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("person") Person person, BindingResult result) {
        personService.addPerson(person);
        
        return "redirect:/index";
    }
    
    @RequestMapping("/delete/{personId}")
    public String deletePerson(@PathVariable("personId") int personId) {
            personService.removePerson(personId);
            
            return "redirect:/index";
    }
}
