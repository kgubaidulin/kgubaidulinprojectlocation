/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.webcontactmanager.dao;

import dv.webcontactmanager.model.Person;
import java.util.List;

/**
 *
 * @author k.gubaidulin
 */
public interface PersonDAO {
    public void addPerson(Person person);
    public List<Person> listPerson();
    public void removePerson(int id);
}
