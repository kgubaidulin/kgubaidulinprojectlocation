/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.webcontactmanager.dao;

import dv.webcontactmanager.model.Person;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 *
 * @author k.gubaidulin
 */
@Repository
public class PersonDAOImpl implements PersonDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void addPerson(Person person) {
        sessionFactory.getCurrentSession().save(person);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Person> listPerson() {
        return sessionFactory.getCurrentSession().createQuery("from Contact").list();
    }

    @Override
    public void removePerson(int id) {
        Person person = (Person)sessionFactory.getCurrentSession().load(Person.class, id);
        if (person != null) {
            sessionFactory.getCurrentSession().delete(person);
        }
    }
    
}
