/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.webcontactmanager.service;

import dv.webcontactmanager.dao.PersonDAO;
import dv.webcontactmanager.model.Person;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author k.gubaidulin
 */
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDAO personDAO;
    
    @Override
    @Transactional
    public void addPerson(Person person) {
        personDAO.addPerson(person);
    }

    @Override
    @Transactional
    public List<Person> listPerson() {
        return personDAO.listPerson();
    }

    @Override
    @Transactional
    public void removePerson(int id) {
        personDAO.removePerson(id);
    }
}
