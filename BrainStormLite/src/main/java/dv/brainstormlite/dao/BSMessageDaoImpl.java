/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.brainstormlite.dao;

import dv.brainstormlite.model.BSMessage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class BSMessageDaoImpl implements BSMessageDao {

    private final List<BSMessage> list;

    public BSMessageDaoImpl() {
        this.list = new ArrayList<>();
        this.fill();
    }
    
    private void fill() {
        Date today = Calendar.getInstance().getTime();
        
        BSMessage item1 = new BSMessage();
        item1.setId(1);
        item1.setAuthor("Fedor");
        item1.setDate(today);
        item1.setText("Some brain message");
        
        BSMessage item2 = new BSMessage();
        item2.setId(2);
        item2.setAuthor("Daria");
        item2.setDate(today);
        item2.setText("Another brain message");
        
        list.add(item1);
        list.add(item2);
    }
    
    @Override
    public List<BSMessage> getAll() {
        return this.list;
    }

    @Override
    public BSMessage getById(int id) {
        
        for(BSMessage item : list) {
            int curId = item.getId();
            if (curId == id) {
                return item;
            }
        }
        return null;
    }

    @Override
    public boolean Add(BSMessage msg) {
        int id = msg.getId();
        for(BSMessage item : list) {
            int curId = item.getId();
            if (curId > id) {
                id = curId;
            }
        }
        msg.setId(id);
        msg.setDate(Calendar.getInstance().getTime());
        return list.add(msg);
    }
    
}
