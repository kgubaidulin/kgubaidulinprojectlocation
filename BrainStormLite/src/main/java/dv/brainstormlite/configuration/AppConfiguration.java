/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.brainstormlite.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author k.gubaidulin
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "dv.brainstormlite")
public class AppConfiguration {
    
}
