/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.brainstormlite.service;

import dv.brainstormlite.model.BSMessage;
import java.util.List;

/**
 *
 * @author k.gubaidulin
 */
public interface BSMessageService {
    List<BSMessage> getAll();
    BSMessage getById(int id);
    boolean Add(BSMessage msg);
}
