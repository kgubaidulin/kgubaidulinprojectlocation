/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.brainstormlite.service;

import dv.brainstormlite.dao.BSMessageDaoImpl;
import dv.brainstormlite.model.BSMessage;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BSMessageServiceImpl implements BSMessageService {

    @Autowired
    BSMessageDaoImpl dao;
    
    @Override
    public List<BSMessage> getAll() {
        return dao.getAll();
    }

    @Override
    public BSMessage getById(int id) {
        return dao.getById(id);
    }

    @Override
    public boolean Add(BSMessage msg) {
        return dao.Add(msg);
    }
    
}
