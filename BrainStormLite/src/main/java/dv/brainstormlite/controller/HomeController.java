/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dv.brainstormlite.controller;

import dv.brainstormlite.model.BSMessage;
import dv.brainstormlite.service.BSMessageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    
    @Autowired
    BSMessageService service;
    
    @RequestMapping(value = "/bsmessage")
    public List<BSMessage> getAll() {
        return service.getAll();
    }
    
    @RequestMapping(value = "/bsmessage/{id}")
    public BSMessage getById(@PathVariable("id") int id) {
        return service.getById(id);
    }
    
    @RequestMapping(value = "/bsmessage/", method = RequestMethod.POST)
    public @ResponseBody boolean Add(@RequestBody BSMessage msg) {
        return service.Add(msg);
    }
    
}
