<%-- 
    Document   : index
    Created on : 14.03.2016, 16:25:18
    Author     : k.gubaidulin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BrainStormLite</title>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.js"></script>
        <script src="<c:url value='/js/app.js' />"></script>
        <script src="<c:url value='/js/service/BSMessageService.js' />"></script>
        <script src="<c:url value='/js/controller/BSMessageController.js' />"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="<c:url value='/css/app.css' />" rel="stylesheet"></link>
    </head>
    <body ng-app="myApp" ng-controller="BSMessageController as ctrl">
        <h1>BrainStormLite!</h1>
        
        <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="messageForm" class="form-horizontal">
                
                <input type="hidden" ng-model="ctrl.message.id" />
                <input type="hidden" ng-model="ctrl.message.date" />
                
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="author">Author</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.message.author" id="author" class="username form-control input-sm" placeholder="Enter the author of the message" required ng-minlength="3"/>
                            <div class="has-error" ng-show="messageForm.$dirty">
                                <span ng-show="messageForm.author.$error.required">This is a required field</span>
                                <span ng-show="messageForm.author.$error.minlength">Minimum length required is 3</span>
                                <span ng-show="messageForm.author.$invalid">This field is invalid </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="text">Text</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.message.text" id="text" class="form-control input-sm" placeholder="Enter the text."/>
                        </div>
                    </div>
                </div>
 
                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit"  value="Add" class="btn btn-primary btn-sm" ng-disabled="messageForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="messageForm.$pristine">Reset Form</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel panel-default"></div>
        <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of messages</span></div>
              <div class="tablecontainer" >
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Author</th>
                              <th>Date</th>
                              <th>Message</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="m in ctrl.messages">
                              <td><span ng-bind="m.id"></span></td>
                              <td><span ng-bind="m.author"></span></td>
                              <td><span ng-bind="m.date"></span></td>
                              <td><span ng-bind="m.text"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
    </body>
</html>
