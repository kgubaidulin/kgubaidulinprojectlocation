'use strict';

angular.module('myApp').controller('BSMessageController'
, ['$scope', 'BSMessageService', function($scope, BSMessageService){
        var self = this;
        self.message = {id:null,date:null,author:'',text:''};
        self.messages = [];
        self.getAll = function() {
            BSMessageService.getAll().then(
                    function(d){
                        self.messages = d;
                    },
                    function(errResponse){
                        //console.error('Error while get all bsmessages');
                    }
                );
        };
        
        self.add = function(bsmsg) {
            BSMessageService.add(bsmsg).
                    then(
                        self.getAll, function(errResponse) {
                            //console.error('Error while creating bsmessages');
                        }
                    );
        };
        
        self.getAll();
        
        self.submit = function() {
            self.add(self.message);
            //self.reset();
        };
        
        self.reset = function(){
            self.message = {id:null,date:null,author:'',text:''};
            $scope.messageForm.$setPristine(); //reset Form
        };
}]);


