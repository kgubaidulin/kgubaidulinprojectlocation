'use strict';

angular.module('myApp').factory('BSMessageService'
, ['$http', '$q', function($http, $q){
    return {
        getAll: function() {
            return $http.get('/BrainStormLite/bsmessage/').
                    then(
                        function(response){
                            return response.data;
                        }, 
                        function(errResponse){
                            //console.error('Error while fetching bsmessage');
                            return $q.reject(errResponse);
                        }
                    );
        },
        add: function (bsmsg) {
            return $http.post('/BrainStormLite/bsmessage/', bsmsg).
                    then(
                        function(response) {
                            return response.data;
                        },
                        function(errResponce) {
                            //console.error('Error while creating msg');
                            return $q.reject(errResponce);
                        }      
                    );
        }
    };   
}]);